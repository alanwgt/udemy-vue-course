export const fruitMixin = {
    data() {
        return {
            fruits: ["Apple", "Banana", "Mango", "Melon"],
            filterText: ""
        };
    },
    computed: {
        filteredfruits() {
            return this.fruits.filter(e => e.match(this.filterText));
        },
    },
    created() {
        console.log("created");
    }
};