// const cmp = Vue.component("my-cmp", {
const cmp = {
    data: function() {
        return {
            status: "Critical"
        };
    },
    template: "<p>Status Server: {{ status }} (<button @click='changeStatus'>Change</button>)</p>",
    methods: {
        changeStatus: function() {
            this.status = "Normal";
        }
    }
};

new Vue({
    el: "#app",
    components: {
        "my-cmp": cmp
    }
});

new Vue({
    el: "#app2"
});