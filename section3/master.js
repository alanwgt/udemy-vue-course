const data = {
    showElementP: true,
    ingredients: ["meat", "fruit", "cookies"],
    persons: [
        { name: "Max", age: 27, color: "red" },
        { name: "Anna", age: "unknown", color: "blue" },
    ],
};

new Vue({
    el: "#app",
    data,
});