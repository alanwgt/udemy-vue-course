const MAX_PLAYER_DAMAGE = 10;
const MIN_PLAYER_DAMAGE = 3;
const MAX_MONSTER_DAMAGE = 12;
const MIN_MONSTER_DAMAGE = 5;
const SPECIAL_ATTACK_MULTIPLIER = 2;
const HEAL_AMOUNT = 10;
const NEW_GAME_MESSAGE = "Do you want to start a new game?";
const PLAYER_WON_MESSAGE = "Yaay! You won! " + NEW_GAME_MESSAGE;
const PLAYER_LOOSE_MESSAGE = "Aww, you lost :( " + NEW_GAME_MESSAGE;

const randomBetween = function(min, max, mult) {
    return Math.max(Math.floor(Math.random() * (max * mult)) + 1, (min * mult));
};

const getPlayerDamage = function(mult=1) {
    return randomBetween(MIN_PLAYER_DAMAGE, MAX_PLAYER_DAMAGE, mult);
};

const getMonsterDamage = function(mult=1) {
    return randomBetween(MIN_MONSTER_DAMAGE, MAX_MONSTER_DAMAGE, mult);
};

const data = {
    playerHealth: 100,
    monsterHealth: 100,
    isRunning: false,
    turns: []
};

const start = function() {
    this.isRunning = true;
    this.playerHealth = this.monsterHealth = 100;
    this.turns = [];
};

const monsterAttack = function() {
    const monsterDamage = getMonsterDamage();
    this.turns.unshift({
        player: false,
        text: `Monster hits Player for ${monsterDamage} of damage`
    });
    this.playerHealth -= monsterDamage;
};

const _attack = function(mult=1) {
    const playerDamage = getPlayerDamage(mult);
    this.turns.unshift({
        player: true,
        text: `Player hits Monster${mult>1?" hard":""} for ${playerDamage} of damage`
    });
    this.monsterHealth -= playerDamage;;
    if (this.checkEndGame()) {
        return;
    }
    this.monsterAttack();
    this.checkEndGame();
};

const attack = function() {
    this._attack();
};

const specialAttack = function() {
    this._attack(SPECIAL_ATTACK_MULTIPLIER);
};

const heal = function() {
    if (this.playerHealth <= 90) {
        this.playerHealth += HEAL_AMOUNT;
    } else {
        this.playerHealth = 100;
    } 
    this.turns.unshift({
        player: true,
        text: `Player heals for ${HEAL_AMOUNT}`
    });
    this.monsterAttack();
};

const giveUp = function() {
    this.isRunning = false;
};

const checkEndGame = function() {
    if (this.monsterHealth <= 0) {
        if (confirm(PLAYER_WON_MESSAGE)) {
            this.start();
        } else {
            this.isRunning = false;
        }
        return true;
    } else if (this.playerHealth <=0) {
        if (confirm(PLAYER_LOOSE_MESSAGE)) {
            this.start();
        } else {
            this.isRunning = false;
        }
        return true;
    }
    return false;
};

const endGame = function(message) {
    this.isRunning = false;
    alert(message);
};

const methods = {
    start,
    attack,
    specialAttack,
    heal,
    giveUp,
    endGame,
    checkEndGame,
    monsterAttack,
    _attack,
};

new Vue({
    el: "#app",
    data,
    methods,
});