import Vue from 'vue';
import App from './App.vue';

import router from './router';
import store from './store';
import axios from "axios";

axios.defaults.baseURL = "https://vuejs-http-29b2d.firebaseio.com";
// axios.defaults.headers.common["Authorization"] = "Bearer hqiuheiuqwheiuwqei";
axios.defaults.headers.get["Accept"] = "application/json";

const reqInterceptor = axios.interceptors.request.use(config => {
  console.log("Request Interceptor: %o", config);
  return config;
});

const resInterceptor = axios.interceptors.response.use(res => {
  console.log("Response Interceptor: %o", res);
  return res;
});

// axios.interceptors.request.eject(reqInterceptor);
// axios.interceptors.response.eject(resInterceptor);

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
