import axios from "axios";

const instance = axios.create({
    baseURL: "https://vuejs-http-29b2d.firebaseio.com"
});

// instance.defaults.headers...

export default instance;